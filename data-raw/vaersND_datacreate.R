﻿# source 1
# r - Convert column classes in data.table - Stack Overflow answered by Matt Dowle on Dec 27 2013. See \url{https://stackoverflow.com/questions/7813578/convert-column-classes-in-data-table}.

# Data source \url{https://vaers.hhs.gov/data/index}


library("data.table")
library("anytime")

# Vax
vax <- list.files(path = "/vaers_data/raw/ND/", pattern = "vax", full.names = TRUE, ignore.case = TRUE)

vaersND_vax <- fread(vax, colClasses = "character", na.strings = getOption("datatable.na.strings", c("NA", "", "N/A")))

# changing column to numeric class
change_class1 <- "VAERS_ID"
for (col in change_class1) set(vaersND_vax, j = col, value = as.numeric(vaersND_vax[[col]])) # Source 1

save(vaersND_vax, file = "/vaersND/data/vaersND_vax.RData", compress = "xz")



# Symptoms
symptoms <- list.files(path = "/vaers_data/raw/ND/", pattern = "symptoms", full.names = TRUE, ignore.case = TRUE)

vaersND_symptoms <- fread(symptoms, colClasses = "character", na.strings = getOption("datatable.na.strings", c("NA", "", "N/A")))

change_class2 <- c("VAERS_ID", "SYMPTOMVERSION1", "SYMPTOMVERSION2", "SYMPTOMVERSION3", "SYMPTOMVERSION4", "SYMPTOMVERSION5")
for (col in change_class2) set(vaersND_symptoms, j = col, value = as.numeric(vaersND_symptoms[[col]])) # Source 1

save(vaersND_symptoms, file = "/vaersND/data/vaersND_symptoms.RData", compress = "xz")



# Data
data <- list.files(path = "/vaers_data/raw/ND/", pattern = "data", full.names = TRUE, ignore.case = TRUE)

vaersND_data <- fread(data, colClasses = "character", na.strings = getOption("datatable.na.strings", c("NA", "", "N/A")))

change_class <- c("RECVDATE", "RPT_DATE", "DATEDIED", "VAX_DATE", "ONSET_DATE", "TODAYS_DATE")
for (col in change_class) set(vaersND_data, j = col, value = anydate(vaersND_data[[col]])) # Source 1

change_class3 <- c("VAERS_ID", "AGE_YRS", "CAGE_YR", "CAGE_MO", "HOSPDAYS", "NUMDAYS", "FORM_VERS")
for (col in change_class3) set(vaersND_data, j = col, value = as.numeric(vaersND_data[[col]])) # Source 1

save(vaersND_data, file = "/vaersND/data/vaersND_data.RData", compress = "xz")
