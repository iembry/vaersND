﻿# vaersND

Non-Domestic Vaccine Adverse Event Reporting System (VAERS) data for 1990 - Present.

"Established in 1990, the Vaccine Adverse Event Reporting System (VAERS) is a national early warning system to detect possible safety problems in U.S.-licensed vaccines. VAERS is co-managed by the Centers for Disease Control and Prevention (CDC) and the U.S. Food and Drug Administration (FDA). VAERS accepts and analyzes reports of adverse events (possible side effects) after a person has received a vaccination. Anyone can report an adverse event to VAERS. Healthcare professionals are required to report certain adverse events and vaccine manufacturers are required to report all adverse events that come to their attention.

VAERS is a passive reporting system, meaning it relies on individuals to send in reports of their experiences to CDC and FDA. VAERS is not designed to determine if a vaccine caused a health problem, but is especially useful for detecting unusual or unexpected patterns of adverse event reporting that might indicate a possible safety problem with a vaccine. This way, VAERS can provide CDC and FDA with valuable information that additional work and evaluation is necessary to further assess a possible safety concern." Source: [VAERS](https://vaers.hhs.gov/about.html).

For information about vaccination$/immunization$ hazards, visit [https://www.questionuniverse.com/rethink.html#vaccine](https://www.questionuniverse.com/rethink.html#vaccine), [https://www.ecoccs.com/healing.html#vaccines](https://www.ecoccs.com/healing.html#vaccines), and [https://www.questionuniverse.com/rethink_current_crisis.html#cov_vaccin](https://www.questionuniverse.com/rethink_current_crisis.html#cov_vaccin).


The data sets are presented as is i.e., they have not been cleaned up.



# Installation of the full, raw data sets

```R
devtools::install_git("https://gitlab.com/iembry/vaersND.git", build_vignettes = TRUE)
```


# Installation from Irucka's drat repository

```R
install.load::load_package("drat")
# load needed package using the load_package function from the install.load package (it is assumed that you have already installed these packages)

drat::addRepo("iembry", "https://iembry.gitlab.io/drat/")

install.packages("vaersND", repo = "https://iembry.gitlab.io/drat")
```


# Examples
```R
# Please refer to the vignette for more examples.


install.load::load_package("vaersND", "dplyr", "data.table")
# load needed packages using the load_package function from the install.load package (it is assumed that you have already installed these packages)



## A. load vaersND_data

data(vaersND_data)

setkey(vaersND_data, VAERS_ID)


# 1) How many reports for each sex?

# use count from dplyr
count(vaersND_data, SEX)

# use .N and by from data.table
vaersND_data[, .N, by = SEX]



# 2) Identify the VAERS_IDs for males only.

vaersND_data[SEX == "M"][, 1]




## B. load vaersND_symptoms

data(vaersND_symptoms)

setkey(vaersND_symptoms, VAERS_ID)


# 1) How many reports of autism for SYMPTOM1?

vaersND_symptoms[SYMPTOM1 == "Autism", .N]




## C. load vaersND_vax

data(vaersND_vax)

setkey(vaersND_vax, VAERS_ID)


# 1) What are the counts for each of the VAX_TYPEs?

# use count from dplyr
count(vaersND_vax, VAX_TYPE)

# use .N and by from data.table & sort by VAX_TYPE
vaersND_vax[, .N, by = VAX_TYPE][order(VAX_TYPE)]



# 2) How many reports of MMR as the VAX_TYPE?

vaersND_vax[VAX_TYPE == "MMR", .N]



# 3) How many reports of COVID19 as the VAX_TYPE?

vaersND_vax[VAX_TYPE == "COVID19", .N]



# 3) a) How many people have died from the COVID19 vaccine?

data_vax <- vaersND_data[vaersND_vax]

setkey(data_vax, VAERS_ID)

data_vax[VAX_TYPE == "COVID19" & DIED == "Y", .N]
```



# VAERS Data Disclaimer
[https://vaers.hhs.gov/data.html](https://vaers.hhs.gov/data.html) (The content below is from this URL and is current as of 28 March 2021):

"VAERS accepts reports of adverse events and reactions that occur following vaccination. Healthcare providers, vaccine manufacturers, and the public can submit reports to the system. While very important in monitoring vaccine safety, VAERS reports alone cannot be used to determine if a vaccine caused or contributed to an adverse event or illness. The reports may contain information that is incomplete, inaccurate, coincidental, or unverifiable. In large part, reports to VAERS are voluntary, which means they are subject to biases. This creates specific limitations on how the data can be used scientifically. Data from VAERS reports should always be interpreted with these limitations in mind.

The strengths of VAERS are that it is national in scope and can quickly provide an early warning of a safety problem with a vaccine. As part of CDC and FDA’s multi-system approach to post-licensure vaccine safety monitoring, VAERS is designed to rapidly detect unusual or unexpected patterns of adverse events, also known as “safety signals.” If a safety signal is found in VAERS, further studies can be done in safety systems such as the CDC’s Vaccine Safety Datalink (VSD) or the Clinical Immunization Safety Assessment (CISA) project. These systems do not have the same scientific limitations as VAERS, and can better assess health risks and possible connections between adverse events and a vaccine.

Key considerations and limitations of VAERS data:

*Vaccine providers are encouraged to report any clinically significant health problem following vaccination to VAERS, whether or not they believe the vaccine was the cause.
Reports may include incomplete, inaccurate, coincidental and unverified information.
*The number of reports alone cannot be interpreted or used to reach conclusions about the existence, severity, frequency, or rates of problems associated with vaccines.
*VAERS data is limited to vaccine adverse event reports received between 1990 and the most recent date for which data are available.
*VAERS data do not represent all known safety information for a vaccine and should be interpreted in the context of other scientific information."
