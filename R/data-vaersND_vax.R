#' Non-Domestic Vaccine Adverse Event Reporting System (VAERS) vaccine data for
#' 1990 - Present
#'
#' A table containing "the remaining vaccine information (e.g., vaccine name,
#' manufacturer, lot number, route, site, and number of previous doses
#' administered), for each of the vaccines listed in Box 13 of the VAERS form.
#' There is a matching record in this file with the VAERSDATA file identified
#' by VAERS_ID."
#'
#'
#'
#' @format A data.table data frame with 144727 rows and 8 variables
#' \describe{
#' \item{VAERS_ID}{VAERS Identification Number}
#' \item{VAX_TYPE}{Administered Vaccine Type}
#' \item{VAX_MANU}{Vaccine Manufacturer}
#' \item{VAX_LOT}{Manufacturer's Vaccine Lot}
#' \item{VAX_DOSE_SERIES}{Dose number in series}
#' \item{VAX_ROUTE}{Vaccination Route}
#' \item{VAX_SITE}{Vaccination Site}
#' \item{VAX_NAME}{Vaccination Name}
#' }
#'
#'
#' @references
#' US Centers for Disease Control and Prevention (CDC) and the US Food and Drug Administration (FDA) Vaccine Adverse Event Reporting System (VAERS) \url{https://vaers.hhs.gov/} and \url{https://vaers.hhs.gov/docs/VAERSDataUseGuide_November2020.pdf}.
#'
#'
#'
"vaersND_vax"
#> [1] "vaersND_vax"
